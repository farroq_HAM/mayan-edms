# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Nurgül Özkan <nurgulozkan@hotmail.com>, 2017
# serhatcan77 <serhat_can@yahoo.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-26 22:24-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Caner Başaran <basaran.caner@protonmail.com>\n"
"Language-Team: Turkish (Turkey) (http://www.transifex.com/rosarior/mayan-edms/language/tr_TR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr_TR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:21 permissions.py:7
msgid "Smart settings"
msgstr "Akıllı ayarlar"

#: apps.py:29
msgid "Setting count"
msgstr "Ayar sayısı"

#: apps.py:33
msgid "Name"
msgstr "Ad"

#: apps.py:37
msgid "Value"
msgstr "Değer"

#: apps.py:40
msgid "Overrided by environment variable?"
msgstr ""

#: apps.py:41
msgid "Yes"
msgstr "Evet"

#: apps.py:41
msgid "No"
msgstr "Hayır"

#: forms.py:12
msgid "Enter the new setting value."
msgstr ""

#: forms.py:31
msgid "Value must be properly quoted."
msgstr ""

#: forms.py:40
#, python-format
msgid "\"%s\" not a valid entry."
msgstr ""

#: links.py:12 links.py:16
msgid "Settings"
msgstr "Ayarlar"

#: links.py:21
msgid "Namespaces"
msgstr ""

#: links.py:25
msgid "Edit"
msgstr "Düzenle"

#: permissions.py:10
msgid "Edit settings"
msgstr ""

#: permissions.py:13
msgid "View settings"
msgstr "Ayarları görüntüle"

#: views.py:18
msgid "Setting namespaces"
msgstr "Alan adları ayarı"

#: views.py:34
msgid ""
"Settings inherited from an environment variable take precedence and cannot "
"be changed in this view. "
msgstr ""

#: views.py:37
#, python-format
msgid "Settings in namespace: %s"
msgstr "%sİsim alanındaki ayarlar: "

#: views.py:45
#, python-format
msgid "Namespace: %s, not found"
msgstr "Ad alanı: %s, bulunamadı"

#: views.py:60
msgid "Setting updated successfully."
msgstr ""

#: views.py:68
#, python-format
msgid "Edit setting: %s"
msgstr ""
