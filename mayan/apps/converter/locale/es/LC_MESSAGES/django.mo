��    )      d  ;   �      �  	   �  :   �  	   �     �  !        $     ?     D  C   K     �     �  @   �     �       U     (   q  1   �     �     �  #   �  '        +     4     ;     @     S     n  r   t     �     �     �               -     <  �   L     �     �     �       d    
   �	  G   �	     �	     �	     �	     
     8
     A
  G   H
     �
     �
  D   �
     �
       \   !  :   ~  @   �     �       0     )   N     x     �     �     �  *   �     �  u   �     N     a     g     x     �     �     �  �   �     E     _     o     �                           !          	                                      #      '       %   &       
             )                                       $      (           "                                Arguments Configuration options for the graphics conversion backend. Converter Create new transformation Create new transformation for: %s Create new transformations Crop Delete Delete transformation "%(transformation)s" for: %(content_object)s? Delete transformations Edit Edit transformation "%(transformation)s" for: %(content_object)s Edit transformations Enter a valid YAML value. Enter the arguments for the transformation as a YAML dictionary. ie: {"degrees": 180} Exception determining PDF page count; %s Exception determining page count using Pillow; %s Flip Gaussian blur Graphics conversion backend to use. LibreOffice not installed or not found. Line art Mirror Name No transformations Not an office file format. Order Order in which the transformations will be executed. If left unchanged, an automatic order value will be assigned. Resize Rotate Rotate 180 degrees Rotate 270 degrees Rotate 90 degrees Transformation Transformations Transformations allow changing the visual appearance of documents without making permanent changes to the document file themselves. Transformations for: %s Unsharp masking View existing transformations Zoom Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-09-27 02:17+0000
Last-Translator: Roberto Rosario
Language-Team: Spanish (http://www.transifex.com/rosarior/mayan-edms/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Argumentos Opciones de configuración para el backend de conversión de gráficos. Convertidor Crear nueva transformación Crear transformación para :%s Crear nuevas transformaciones Recortar Borrar ¿Borrar transformación "%(transformation)s" para: %(content_object)s? Borrar transformaciones Editar Editar transformación "%(transformation)s" para: %(content_object)s Editar transformaciones Entre un valor YAML. Entre el argumento de la transformación como un diccionario YAML. Ejemplo: {"degrees": 180} Excepción determinando el número de páginas del PDF; %s Excepción determinando el número de páginas usando Pillow; %s Dar la vuelta Desenfoque gaussiano Módulo de conversión de gráficos a ser usado. LibreOffice no instalado o no encontrado. Arte lineal Espejo Nombre Sin transformaciones No es un formato de archivo de la oficina. Orden Orden de ejecución de las transformaciones. Si lo deja en blanco, un valor de orden sera asignado automáticamente.  Cambiar el tamaño Girar Girar 180 grados Girar 270 grados Girar 90 grados Transformación Transformaciones Las transformaciones permiten cambiar la apariencia visual de los documentos sin realizar cambios permanentes en el archivo del documento. Transformaciones para: %s Unsharp masking Ver transformaciones existentes Ampliar 