# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-26 22:23-0400\n"
"PO-Revision-Date: 2018-09-27 02:30+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Chinese (China) (http://www.transifex.com/rosarior/mayan-edms/language/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: apps.py:115 apps.py:268 events.py:7 menus.py:10 models.py:238
#: permissions.py:7 queues.py:18 settings.py:12 statistics.py:231
msgid "Documents"
msgstr "文档"

#: apps.py:136
msgid "Create a document type"
msgstr ""

#: apps.py:138
msgid ""
"Every uploaded document must be assigned a document type, it is the basic "
"way Mayan EDMS categorizes documents."
msgstr ""

#: apps.py:157
msgid "Versions comment"
msgstr ""

#: apps.py:160
msgid "Versions encoding"
msgstr ""

#: apps.py:163
msgid "Versions mime type"
msgstr ""

#: apps.py:166
msgid "Versions timestamp"
msgstr ""

#: apps.py:231 apps.py:248 apps.py:255 apps.py:283 apps.py:298 apps.py:324
msgid "Thumbnail"
msgstr ""

#: apps.py:240 apps.py:307 forms.py:186 links.py:84
msgid "Pages"
msgstr "页面"

#: apps.py:262
msgid "Type"
msgstr ""

#: apps.py:275 models.py:761
msgid "Enabled"
msgstr ""

#: apps.py:330 links.py:366 views/document_views.py:846
msgid "Duplicates"
msgstr ""

#: dashboard_widgets.py:24
msgid "Total pages"
msgstr ""

#: dashboard_widgets.py:46
msgid "Total documents"
msgstr ""

#: dashboard_widgets.py:65 views/document_views.py:168
msgid "Documents in trash"
msgstr ""

#: dashboard_widgets.py:84 links.py:352 links.py:357 permissions.py:55
#: views/document_type_views.py:71
msgid "Document types"
msgstr ""

#: dashboard_widgets.py:103
msgid "New documents this month"
msgstr ""

#: dashboard_widgets.py:116
msgid "New pages this month"
msgstr ""

#: events.py:10
msgid "Document created"
msgstr ""

#: events.py:13
msgid "Document downloaded"
msgstr ""

#: events.py:16
msgid "New version uploaded"
msgstr ""

#: events.py:19
msgid "Document properties edited"
msgstr ""

#: events.py:23
msgid "Document type changed"
msgstr ""

#: events.py:27
msgid "Document type created"
msgstr ""

#: events.py:31
msgid "Document type edited"
msgstr ""

#: events.py:34
msgid "Document version reverted"
msgstr ""

#: events.py:37
msgid "Document viewed"
msgstr ""

#: forms.py:96
msgid "Quick document rename"
msgstr "快速文档重命名"

#: forms.py:104 forms.py:256
msgid "Preserve extension"
msgstr ""

#: forms.py:106
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open document correctly."
msgstr ""

#: forms.py:149
msgid "Date added"
msgstr "添加日期"

#: forms.py:153 models.py:185
msgid "UUID"
msgstr "UUID"

#: forms.py:155 models.py:209
msgid "Language"
msgstr ""

#: forms.py:157
msgid "Unknown"
msgstr ""

#: forms.py:165
msgid "File mimetype"
msgstr "文件mimetype"

#: forms.py:166 forms.py:171
msgid "None"
msgstr "无"

#: forms.py:169
msgid "File encoding"
msgstr ""

#: forms.py:175 models.py:1006
msgid "File size"
msgstr "文件大小"

#: forms.py:180
msgid "Exists in storage"
msgstr "在存储中存在"

#: forms.py:182
msgid "File path in storage"
msgstr "在存储中的文件路径"

#: forms.py:185 models.py:460 search.py:24 search.py:48
msgid "Checksum"
msgstr "校验码"

#: forms.py:213 models.py:103 models.py:189 models.py:756 search.py:16
#: search.py:35
msgid "Document type"
msgstr "文档类型"

#: forms.py:229
msgid "Compress"
msgstr "压缩"

#: forms.py:231
msgid ""
"Download the document in the original format or in a compressed manner. This"
" option is selectable only when downloading one document, for multiple "
"documents, the bundle will always be downloads as a compressed file."
msgstr ""

#: forms.py:238
msgid "Compressed filename"
msgstr "压缩的文件名"

#: forms.py:241
msgid ""
"The filename of the compressed file that will contain the documents to be "
"downloaded, if the previous option is selected."
msgstr "如果选择了前面选项，压缩文件的文件名将包含要下载的文档"

#: forms.py:258
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open the downloaded "
"document version correctly."
msgstr ""

#: forms.py:270 literals.py:39
msgid "Page range"
msgstr "页范围"

#: forms.py:276
msgid ""
"Page number from which all the transformation will be cloned. Existing "
"transformations will be lost."
msgstr ""

#: links.py:70
msgid "Preview"
msgstr ""

#: links.py:75
msgid "Properties"
msgstr ""

#: links.py:80 links.py:200
msgid "Versions"
msgstr ""

#: links.py:92 links.py:152
msgid "Clear transformations"
msgstr ""

#: links.py:97
msgid "Clone transformations"
msgstr ""

#: links.py:102 links.py:160 links.py:325 links.py:340
msgid "Delete"
msgstr "删除"

#: links.py:106 links.py:164
msgid "Add to favorites"
msgstr ""

#: links.py:111 links.py:168
msgid "Remove from favorites"
msgstr ""

#: links.py:116 links.py:156
msgid "Move to trash"
msgstr ""

#: links.py:122
msgid "Edit properties"
msgstr ""

#: links.py:126 links.py:172
msgid "Change type"
msgstr ""

#: links.py:131 links.py:176
msgid "Advanced download"
msgstr ""

#: links.py:135
msgid "Print"
msgstr ""

#: links.py:139
msgid "Quick download"
msgstr ""

#: links.py:143 links.py:179
msgid "Recalculate page count"
msgstr ""

#: links.py:147 links.py:183
msgid "Restore"
msgstr ""

#: links.py:189
msgid "Download version"
msgstr ""

#: links.py:194 links.py:275 models.py:237 models.py:423 models.py:1040
#: models.py:1070 models.py:1099
msgid "Document"
msgstr ""

#: links.py:205
msgid "Details"
msgstr "细节"

#: links.py:210 views/document_views.py:96
msgid "All documents"
msgstr ""

#: links.py:214 views/document_views.py:885
msgid "Favorites"
msgstr ""

#: links.py:218 views/document_views.py:969
msgid "Recently accessed"
msgstr ""

#: links.py:222 views/document_views.py:993
msgid "Recently added"
msgstr ""

#: links.py:226
msgid "Trash can"
msgstr ""

#: links.py:234
msgid ""
"Clear the graphics representations used to speed up the documents' display "
"and interactive transformations results."
msgstr "清除图像信息有助于加速文档显示和变换结果交互。"

#: links.py:237
msgid "Clear document image cache"
msgstr ""

#: links.py:241 permissions.py:51
msgid "Empty trash"
msgstr ""

#: links.py:250
msgid "First page"
msgstr ""

#: links.py:255
msgid "Last page"
msgstr ""

#: links.py:263
msgid "Previous page"
msgstr ""

#: links.py:269
msgid "Next page"
msgstr ""

#: links.py:281
msgid "Rotate left"
msgstr ""

#: links.py:286
msgid "Rotate right"
msgstr ""

#: links.py:289
msgid "Page image"
msgstr "页面图片"

#: links.py:293
msgid "Reset view"
msgstr ""

#: links.py:299
msgid "Zoom in"
msgstr ""

#: links.py:305
msgid "Zoom out"
msgstr ""

#: links.py:313
msgid "Revert"
msgstr ""

#: links.py:320 views/document_type_views.py:86
msgid "Create document type"
msgstr ""

#: links.py:329 links.py:345
msgid "Edit"
msgstr ""

#: links.py:335
msgid "Add quick label to document type"
msgstr ""

#: links.py:349 models.py:767
msgid "Quick labels"
msgstr ""

#: links.py:361 models.py:1043 models.py:1053 views/document_views.py:865
msgid "Duplicated documents"
msgstr ""

#: links.py:372
msgid "Duplicated document scan"
msgstr ""

#: literals.py:30
msgid "Default"
msgstr ""

#: literals.py:39
msgid "All pages"
msgstr ""

#: models.py:74
msgid "The name of the document type."
msgstr ""

#: models.py:75 models.py:193 models.py:759 search.py:21 search.py:42
msgid "Label"
msgstr ""

#: models.py:79
msgid ""
"Amount of time after which documents of this type will be moved to the "
"trash."
msgstr ""

#: models.py:81
msgid "Trash time period"
msgstr ""

#: models.py:85
msgid "Trash time unit"
msgstr ""

#: models.py:89
msgid ""
"Amount of time after which documents of this type in the trash will be "
"deleted."
msgstr ""

#: models.py:91
msgid "Delete time period"
msgstr ""

#: models.py:96
msgid "Delete time unit"
msgstr ""

#: models.py:104
msgid "Documents types"
msgstr ""

#: models.py:183
msgid ""
"UUID of a document, universally Unique ID. An unique identifiergenerated for"
" each document."
msgstr ""

#: models.py:193
msgid "The name of the document."
msgstr ""

#: models.py:197
msgid "An optional short text describing a document."
msgstr ""

#: models.py:198 search.py:22 search.py:45
msgid "Description"
msgstr "描述"

#: models.py:202
msgid ""
"The server date and time when the document was finally processed and added "
"to the system."
msgstr ""

#: models.py:204 models.py:1046
msgid "Added"
msgstr ""

#: models.py:208
msgid "The dominant language in the document."
msgstr ""

#: models.py:213
msgid "Whether or not this document is in the trash."
msgstr ""

#: models.py:214
msgid "In trash?"
msgstr ""

#: models.py:219
msgid "The server date and time when the document was moved to the trash."
msgstr ""

#: models.py:221
msgid "Date and time trashed"
msgstr ""

#: models.py:225
msgid ""
"A document stub is a document with an entry on the database but no file "
"uploaded. This could be an interrupted upload or a deferred upload via the "
"API."
msgstr ""

#: models.py:228
msgid "Is stub?"
msgstr ""

#: models.py:241
#, python-format
msgid "Document stub, id: %d"
msgstr ""

#: models.py:427
msgid "The server date and time when the document version was processed."
msgstr ""

#: models.py:428
msgid "Timestamp"
msgstr ""

#: models.py:432
msgid "An optional short text describing the document version."
msgstr ""

#: models.py:433
msgid "Comment"
msgstr "评论"

#: models.py:439
msgid "File"
msgstr "文件"

#: models.py:443
msgid ""
"The document version's file mimetype. MIME types are a standard way to "
"describe the format of a file, in this case the file format of the document."
" Some examples: \"text/plain\" or \"image/jpeg\". "
msgstr ""

#: models.py:447 search.py:19 search.py:39
msgid "MIME type"
msgstr "MIME类型"

#: models.py:451
msgid ""
"The document version file encoding. binary 7-bit, binary 8-bit, text, "
"base64, etc."
msgstr ""

#: models.py:453
msgid "Encoding"
msgstr ""

#: models.py:465 models.py:466 models.py:780
msgid "Document version"
msgstr ""

#: models.py:766
msgid "Quick label"
msgstr ""

#: models.py:784
msgid "Page number"
msgstr ""

#: models.py:791 models.py:999 models.py:1032
msgid "Document page"
msgstr ""

#: models.py:792 models.py:1033
msgid "Document pages"
msgstr ""

#: models.py:796
#, python-format
msgid "Page %(page_num)d out of %(total_pages)d of %(document)s"
msgstr "文档%(document)s页%(page_num)d超出%(total_pages)d"

#: models.py:1002
msgid "Date time"
msgstr ""

#: models.py:1004
msgid "Filename"
msgstr "文件名"

#: models.py:1012
msgid "Document page cached image"
msgstr ""

#: models.py:1013
msgid "Document page cached images"
msgstr ""

#: models.py:1052
msgid "Duplicated document"
msgstr ""

#: models.py:1066 models.py:1095
msgid "User"
msgstr "用户"

#: models.py:1076
msgid "Favorite document"
msgstr ""

#: models.py:1077
msgid "Favorite documents"
msgstr ""

#: models.py:1102
msgid "Accessed"
msgstr ""

#: models.py:1109
msgid "Recent document"
msgstr ""

#: models.py:1110
msgid "Recent documents"
msgstr ""

#: permissions.py:10
msgid "Create documents"
msgstr "创建文档"

#: permissions.py:13
msgid "Delete documents"
msgstr "删除文档"

#: permissions.py:16
msgid "Trash documents"
msgstr ""

#: permissions.py:19 views/document_views.py:502
msgid "Download documents"
msgstr "下载文档"

#: permissions.py:22
msgid "Edit documents"
msgstr "编辑文档"

#: permissions.py:25
msgid "Create new document versions"
msgstr "新建文档版本"

#: permissions.py:28
msgid "Edit document properties"
msgstr "编辑文档属性"

#: permissions.py:31
msgid "Print documents"
msgstr ""

#: permissions.py:34
msgid "Restore trashed document"
msgstr ""

#: permissions.py:37
msgid "Execute document modifying tools"
msgstr "执行文档修改工具"

#: permissions.py:41
msgid "Revert documents to a previous version"
msgstr "恢复文档到前一版本"

#: permissions.py:45
msgid "View documents' versions list"
msgstr ""

#: permissions.py:48
msgid "View documents"
msgstr "查看文档"

#: permissions.py:58
msgid "Create document types"
msgstr "创建文档类型"

#: permissions.py:61
msgid "Delete document types"
msgstr "删除文档类型"

#: permissions.py:64
msgid "Edit document types"
msgstr "编辑文档类型"

#: permissions.py:67
msgid "View document types"
msgstr "查看文档类型"

#: queues.py:9
msgid "Converter"
msgstr ""

#: queues.py:12
msgid "Documents periodic"
msgstr ""

#: queues.py:15
msgid "Uploads"
msgstr ""

#: queues.py:23
msgid "Generate document page image"
msgstr ""

#: queues.py:28
msgid "Delete a document"
msgstr ""

#: queues.py:32
msgid "Clean empty duplicate lists"
msgstr ""

#: queues.py:37
msgid "Check document type delete periods"
msgstr ""

#: queues.py:41
msgid "Check document type trash periods"
msgstr ""

#: queues.py:45
msgid "Delete document stubs"
msgstr ""

#: queues.py:50
msgid "Clear image cache"
msgstr ""

#: queues.py:55
msgid "Update document page count"
msgstr ""

#: queues.py:59
msgid "Upload new document version"
msgstr ""

#: settings.py:17
msgid ""
"Path to the Storage subclass to use when storing the cached document image "
"files."
msgstr ""

#: settings.py:26
msgid "Arguments to pass to the DOCUMENT_CACHE_STORAGE_BACKEND."
msgstr ""

#: settings.py:32
msgid ""
"Disables the first cache tier which stores high resolution, non transformed "
"versions of documents's pages."
msgstr ""

#: settings.py:39
msgid ""
"Disables the second cache tier which stores medium to low resolution, "
"transformed (rotated, zoomed, etc) versions of documents' pages."
msgstr ""

#: settings.py:53
msgid "Maximum number of favorite documents to remember per user."
msgstr ""

#: settings.py:59
msgid ""
"Detect the orientation of each of the document's pages and create a "
"corresponding rotation transformation to display it rightside up. This is an"
" experimental feature and it is disabled by default."
msgstr ""

#: settings.py:67
msgid "Default documents language (in ISO639-3 format)."
msgstr ""

#: settings.py:71
msgid "List of supported document languages. In ISO639-3 format."
msgstr ""

#: settings.py:76
msgid ""
"Time in seconds that the browser should cache the supplied document images. "
"The default of 31559626 seconds corresponde to 1 year."
msgstr ""

#: settings.py:95
msgid ""
"Maximum number of recently accessed (created, edited, viewed) documents to "
"remember per user."
msgstr ""

#: settings.py:102
msgid "Maximum number of recently created documents to show."
msgstr ""

#: settings.py:108
msgid "Amount in degrees to rotate a document page per user interaction."
msgstr "每个交互用户中文档页的旋转度数"

#: settings.py:114
msgid "Path to the Storage subclass to use when storing document files."
msgstr ""

#: settings.py:122
msgid "Arguments to pass to the DOCUMENT_STORAGE_BACKEND."
msgstr ""

#: settings.py:126
msgid "Height in pixels of the document thumbnail image."
msgstr ""

#: settings.py:137
msgid ""
"Maximum amount in percent (%) to allow user to zoom in a document page "
"interactively."
msgstr "文档交互页中允许用户放大的最大百分比(%)"

#: settings.py:144
msgid ""
"Minimum amount in percent (%) to allow user to zoom out a document page "
"interactively."
msgstr "文档交互页中允许用户缩小的最小百分比(%)"

#: settings.py:151
msgid "Amount in percent zoom in or out a document page per user interaction."
msgstr "每个交互用户文档页的放大或缩小百分比"

#: statistics.py:16
msgid "January"
msgstr ""

#: statistics.py:16
msgid "February"
msgstr ""

#: statistics.py:16
msgid "March"
msgstr ""

#: statistics.py:16
msgid "April"
msgstr ""

#: statistics.py:16
msgid "May"
msgstr ""

#: statistics.py:17
msgid "June"
msgstr ""

#: statistics.py:17
msgid "July"
msgstr ""

#: statistics.py:17
msgid "August"
msgstr ""

#: statistics.py:17
msgid "September"
msgstr ""

#: statistics.py:17
msgid "October"
msgstr ""

#: statistics.py:18
msgid "November"
msgstr ""

#: statistics.py:18
msgid "December"
msgstr ""

#: statistics.py:235
msgid "New documents per month"
msgstr ""

#: statistics.py:242
msgid "New document versions per month"
msgstr ""

#: statistics.py:249
msgid "New document pages per month"
msgstr ""

#: statistics.py:256
msgid "Total documents at each month"
msgstr ""

#: statistics.py:263
msgid "Total document versions at each month"
msgstr ""

#: statistics.py:270
msgid "Total document pages at each month"
msgstr ""

#: templates/documents/forms/widgets/document_page_carousel.html:16
#, python-format
msgid ""
"\n"
"                    Page %(page_number)s of %(total_pages)s\n"
"                "
msgstr ""

#: templates/documents/forms/widgets/document_page_carousel.html:22
msgid "No pages to display"
msgstr ""

#: views/document_page_views.py:49
#, python-format
msgid "Pages for document: %s"
msgstr ""

#: views/document_page_views.py:104
msgid "Unknown view keyword argument schema, unable to redirect."
msgstr ""

#: views/document_page_views.py:136
msgid "There are no more pages in this document"
msgstr "此文档中无更多页"

#: views/document_page_views.py:153
msgid "You are already at the first page of this document"
msgstr "你已经在此文档的第一页了"

#: views/document_page_views.py:181
#, python-format
msgid "Image of: %s"
msgstr ""

#: views/document_type_views.py:46
#, python-format
msgid "Documents of type: %s"
msgstr ""

#: views/document_type_views.py:64
msgid ""
"Document types are the most basic units of configuration. Everything in the "
"system will depend on them. Define a document type for each type of physical"
" document you intend to upload. Example document types: invoice, receipt, "
"manual, prescription, balance sheet."
msgstr ""

#: views/document_type_views.py:70
msgid "No document types available"
msgstr ""

#: views/document_type_views.py:102
msgid "All documents of this type will be deleted too."
msgstr ""

#: views/document_type_views.py:104
#, python-format
msgid "Delete the document type: %s?"
msgstr ""

#: views/document_type_views.py:120
#, python-format
msgid "Edit document type: %s"
msgstr ""

#: views/document_type_views.py:150
#, python-format
msgid "Create quick label for document type: %s"
msgstr ""

#: views/document_type_views.py:171
#, python-format
msgid "Edit quick label \"%(filename)s\" from document type \"%(document_type)s\""
msgstr ""

#: views/document_type_views.py:196
#, python-format
msgid ""
"Delete the quick label: %(label)s, from document type \"%(document_type)s\"?"
msgstr ""

#: views/document_type_views.py:232
msgid ""
"Quick labels are predetermined filenames that allow the quick renaming of "
"documents as they are uploaded by selecting them from a list. Quick labels "
"can also be used after the documents have been uploaded."
msgstr ""

#: views/document_type_views.py:238
msgid "There are no quick labels for this document type"
msgstr ""

#: views/document_type_views.py:241
#, python-format
msgid "Quick labels for document type: %s"
msgstr ""

#: views/document_version_views.py:48
#, python-format
msgid "Versions of document: %s"
msgstr ""

#: views/document_version_views.py:62
msgid "All later version after this one will be deleted too."
msgstr "此版本以后的所有版本将被删除"

#: views/document_version_views.py:65
msgid "Revert to this version?"
msgstr ""

#: views/document_version_views.py:75
msgid "Document version reverted successfully"
msgstr "文档版本恢复成功"

#: views/document_version_views.py:80
#, python-format
msgid "Error reverting document version; %s"
msgstr "恢复文档版本失败%s"

#: views/document_version_views.py:99
msgid "Download document version"
msgstr ""

#: views/document_version_views.py:165
#, python-format
msgid "Preview of document version: %s"
msgstr ""

#: views/document_views.py:71
#, python-format
msgid "Error retrieving document list: %(exception)s."
msgstr ""

#: views/document_views.py:91
msgid ""
"This could mean that no documents have been uploaded or that your user "
"account has not been granted the view permission for any document or "
"document type."
msgstr ""

#: views/document_views.py:95
msgid "No documents available"
msgstr ""

#: views/document_views.py:109
msgid "Delete the selected document?"
msgstr ""

#: views/document_views.py:130
#, python-format
msgid "Document: %(document)s deleted."
msgstr ""

#: views/document_views.py:138
msgid "Delete the selected documents?"
msgstr ""

#: views/document_views.py:161
msgid ""
"To avoid loss of data, documents are not deleted instantly. First, they are "
"placed in the trash can. From here they can be then finally deleted or "
"restored."
msgstr ""

#: views/document_views.py:166
msgid "There are no documents in the trash can"
msgstr ""

#: views/document_views.py:179
#, python-format
msgid "Document type change request performed on %(count)d document"
msgstr ""

#: views/document_views.py:182
#, python-format
msgid "Document type change request performed on %(count)d documents"
msgstr ""

#: views/document_views.py:189
msgid "Change"
msgstr ""

#: views/document_views.py:191
msgid "Change the type of the selected document"
msgid_plural "Change the type of the selected documents"
msgstr[0] ""

#: views/document_views.py:202
#, python-format
msgid "Change the type of the document: %s"
msgstr ""

#: views/document_views.py:223
#, python-format
msgid "Document type for \"%s\" changed successfully."
msgstr ""

#: views/document_views.py:248
msgid "Only exact copies of this document will be shown in the this list."
msgstr ""

#: views/document_views.py:252
msgid "There are no duplicates for this document"
msgstr ""

#: views/document_views.py:255
#, python-format
msgid "Duplicates for document: %s"
msgstr ""

#: views/document_views.py:284
#, python-format
msgid "Edit properties of document: %s"
msgstr ""

#: views/document_views.py:318
#, python-format
msgid "Preview of document: %s"
msgstr ""

#: views/document_views.py:324
msgid "Restore the selected document?"
msgstr ""

#: views/document_views.py:345
#, python-format
msgid "Document: %(document)s restored."
msgstr ""

#: views/document_views.py:353
msgid "Restore the selected documents?"
msgstr ""

#: views/document_views.py:364
#, python-format
msgid "Move \"%s\" to the trash?"
msgstr ""

#: views/document_views.py:387
#, python-format
msgid "Document: %(document)s moved to trash successfully."
msgstr ""

#: views/document_views.py:400
msgid "Move the selected documents to the trash?"
msgstr ""

#: views/document_views.py:418
#, python-format
msgid "Properties for document: %s"
msgstr ""

#: views/document_views.py:424
msgid "Empty trash?"
msgstr ""

#: views/document_views.py:437
msgid "Trash emptied successfully"
msgstr ""

#: views/document_views.py:500
msgid "Download"
msgstr "下载"

#: views/document_views.py:606
#, python-format
msgid "%(count)d document queued for page count recalculation"
msgstr ""

#: views/document_views.py:609
#, python-format
msgid "%(count)d documents queued for page count recalculation"
msgstr ""

#: views/document_views.py:617
msgid "Recalculate the page count of the selected document?"
msgid_plural "Recalculate the page count of the selected documents?"
msgstr[0] ""

#: views/document_views.py:628
#, python-format
msgid "Recalculate the page count of the document: %s?"
msgstr ""

#: views/document_views.py:644
#, python-format
msgid ""
"Document \"%(document)s\" is empty. Upload at least one document version "
"before attempting to detect the page count."
msgstr ""

#: views/document_views.py:657
#, python-format
msgid "Transformation clear request processed for %(count)d document"
msgstr ""

#: views/document_views.py:660
#, python-format
msgid "Transformation clear request processed for %(count)d documents"
msgstr ""

#: views/document_views.py:668
msgid "Clear all the page transformations for the selected document?"
msgid_plural "Clear all the page transformations for the selected document?"
msgstr[0] ""

#: views/document_views.py:679
#, python-format
msgid "Clear all the page transformations for the document: %s?"
msgstr ""

#: views/document_views.py:694 views/document_views.py:722
#, python-format
msgid ""
"Error deleting the page transformations for document: %(document)s; "
"%(error)s."
msgstr "删除文档: %(document)s变换页数失败%(error)s"

#: views/document_views.py:730
msgid "Transformations cloned successfully."
msgstr ""

#: views/document_views.py:745 views/document_views.py:818
msgid "Submit"
msgstr "提交"

#: views/document_views.py:747
#, python-format
msgid "Clone page transformations for document: %s"
msgstr ""

#: views/document_views.py:821
#, python-format
msgid "Print: %s"
msgstr ""

#: views/document_views.py:856
msgid ""
"Duplicates are documents that are composed of the exact same file, down to "
"the last byte. Files that have the same text or OCR but are not identical or"
" were saved using a different file format will not appear as duplicates."
msgstr ""

#: views/document_views.py:863
msgid "There are no duplicated documents"
msgstr ""

#: views/document_views.py:881
#, python-format
msgid ""
"Favorited documents will be listed in this view. Up to %(count)d documents "
"can be favorited per user. "
msgstr ""

#: views/document_views.py:884
msgid "There are no favorited documents."
msgstr ""

#: views/document_views.py:895
#, python-format
msgid "%(count)d document added to favorites."
msgstr ""

#: views/document_views.py:898
#, python-format
msgid "%(count)d documents added to favorites."
msgstr ""

#: views/document_views.py:905
msgid "Add"
msgstr "新增"

#: views/document_views.py:908
msgid "Add the selected document to favorites"
msgid_plural "Add the selected documents to favorites"
msgstr[0] ""

#: views/document_views.py:921
#, python-format
msgid "Document \"%(instance)s\" is not in favorites."
msgstr ""

#: views/document_views.py:925
#, python-format
msgid "%(count)d document removed from favorites."
msgstr ""

#: views/document_views.py:928
#, python-format
msgid "%(count)d documents removed from favorites."
msgstr ""

#: views/document_views.py:935
msgid "Remove"
msgstr "移除"

#: views/document_views.py:938
msgid "Remove the selected document from favorites"
msgid_plural "Remove the selected documents from favorites"
msgstr[0] ""

#: views/document_views.py:963
msgid ""
"This view will list the latest documents viewed or manipulated in any way by"
" this user account."
msgstr ""

#: views/document_views.py:967
msgid "There are no recently accessed document"
msgstr ""

#: views/document_views.py:987
msgid "This view will list the latest documents uploaded in the system."
msgstr ""

#: views/document_views.py:991
msgid "There are no recently added document"
msgstr ""

#: views/misc_views.py:18
msgid "Clear the document image cache?"
msgstr ""

#: views/misc_views.py:25
msgid "Document cache clearing queued successfully."
msgstr ""

#: views/misc_views.py:31
msgid "Scan for duplicated documents?"
msgstr ""

#: views/misc_views.py:38
msgid "Duplicated document scan queued successfully."
msgstr ""

#: widgets.py:80 widgets.py:84
#, python-format
msgid "Pages: %d"
msgstr ""
