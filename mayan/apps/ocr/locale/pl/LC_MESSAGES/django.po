# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Annunnaky <doublemiu@gmail.com>, 2015
# Wojciech Warczakowski <w.warczakowski@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-26 22:24-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Polish (http://www.transifex.com/rosarior/mayan-edms/language/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: apps.py:71 apps.py:146 apps.py:150 events.py:7 links.py:19 links.py:24
#: permissions.py:7 queues.py:7 settings.py:7
msgid "OCR"
msgstr "OCR"

#: apps.py:121
msgid "Document"
msgstr "Dokument"

#: apps.py:125
msgid "Added"
msgstr "Dodano"

#: apps.py:129 models.py:69
msgid "Result"
msgstr "Wynik"

#: events.py:10
msgid "Document version submitted for OCR"
msgstr ""

#: events.py:14
msgid "Document version OCR finished"
msgstr ""

#: forms.py:32 forms.py:75
msgid "Contents"
msgstr "Zawartość"

#: forms.py:67
#, python-format
msgid "Page %(page_number)d"
msgstr ""

#: links.py:29 links.py:32
msgid "Submit for OCR"
msgstr "Zgłoś do OCR"

#: links.py:36
msgid "Setup OCR"
msgstr ""

#: links.py:41
msgid "OCR documents per type"
msgstr ""

#: links.py:46 links.py:50 views.py:144
msgid "OCR errors"
msgstr "Błędy OCR"

#: links.py:55
msgid "Download OCR text"
msgstr ""

#: models.py:20
msgid "Document type"
msgstr "Typ dokumentów"

#: models.py:24
msgid "Automatically queue newly created documents for OCR."
msgstr "Automatically queue newly created documents for OCR."

#: models.py:30
msgid "Document type settings"
msgstr ""

#: models.py:31
msgid "Document types settings"
msgstr ""

#: models.py:42
msgid "Document page"
msgstr "Strona dokumentu"

#: models.py:46
msgid "The actual text content extracted by the OCR backend."
msgstr ""

#: models.py:47
msgid "Content"
msgstr "Zawartość"

#: models.py:53
msgid "Document page OCR content"
msgstr ""

#: models.py:54
msgid "Document pages OCR contents"
msgstr ""

#: models.py:64
msgid "Document version"
msgstr "Wersja dokumentu"

#: models.py:67
msgid "Date time submitted"
msgstr "Data dodania"

#: models.py:73
msgid "Document version OCR error"
msgstr ""

#: models.py:74
msgid "Document version OCR errors"
msgstr ""

#: permissions.py:10
msgid "Submit documents for OCR"
msgstr "Prześlij dokument do OCR"

#: permissions.py:14
msgid "View the transcribed text from document"
msgstr ""

#: permissions.py:18
msgid "Change document type OCR settings"
msgstr ""

#: queues.py:9
msgid "Document version OCR"
msgstr ""

#: settings.py:11
msgid "Full path to the backend to be used to do OCR."
msgstr ""

#: settings.py:20
msgid "Set new document types to perform OCR automatically by default."
msgstr ""

#: views.py:44
#, python-format
msgid "OCR result for document: %s"
msgstr ""

#: views.py:66
#, python-format
msgid "OCR result for document page: %s"
msgstr ""

#: views.py:81
msgid "Submit the selected document to the OCR queue?"
msgid_plural "Submit the selected documents to the OCR queue?"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: views.py:95
msgid "Submit all documents of a type for OCR"
msgstr ""

#: views.py:107
#, python-format
msgid "%(count)d documents of type \"%(document_type)s\" added to the OCR queue."
msgstr ""

#: views.py:133
#, python-format
msgid "Edit OCR settings for document type: %s"
msgstr ""

#: views.py:162
#, python-format
msgid "OCR errors for document: %s"
msgstr ""
